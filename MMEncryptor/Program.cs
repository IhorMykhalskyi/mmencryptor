﻿using System;
using MMEncryptor.Forms;
using System.Windows.Forms;
using System.IO;


namespace MMEncryptor
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args == null || args.Length == 0)
            {
                Application.Run(new MainForm());
            }
            else if (args.Length == 1 && IsMmeFile(args[0]))
            {
                Application.Run(new MainForm(args[0]));
            }
            else
            {
                string containerName;
                if (args.Length == 1)
                {
                    containerName = Path.ChangeExtension(args[0], Constants.Constants.MME_FILE_EXTENTION);
                }
                else
                {
                    string parent = Path.GetDirectoryName(args[0]);
                    string shortContainerName = Path.ChangeExtension(Path.GetFileName(parent), Constants.Constants.MME_FILE_EXTENTION);
                    containerName = Path.Combine(parent, shortContainerName);
                }
                Application.Run(new MainForm(containerName, args));
            } 
        }

        private static bool IsMmeFile(string path)
        {
            return string.Equals(Path.GetExtension(path), Constants.Constants.MME_FILE_EXTENTION);
        }
    }
}
