﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using constants = MMEncryptor.Constants.Constants;

namespace MMEncryptor.Forms
{
    public partial class CreateContainerForm : Form
    {
        public string ContainerName { get; private set; }
        public string[] FilesToEncrypt { get; private set; }

        public CreateContainerForm()
        {
            InitializeComponent();
            CreateButton.Enabled = false;
        }

        private void ChooseButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog fileDialog = new OpenFileDialog())
            {
                fileDialog.Multiselect = true;
                if(fileDialog.ShowDialog() == DialogResult.OK)
                {
                    FilesToEncrypt = fileDialog.FileNames;
                    ChoosenFilesTextBox.Text = ConvertStringArrayToString(fileDialog.FileNames);
                    UpdateCreateButton();               
                }
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ContainerName = null;
            FilesToEncrypt = null;
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void buttonChooseFileToSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.DefaultExt = constants.MME_FILE_EXTENTION.Remove(0,1);
            sf.AddExtension = true;
            sf.Filter = constants.MME_FILE_FILTER;
            if (sf.ShowDialog() == DialogResult.OK)
            {
                ContainerName = sf.FileName;
                NewFileNameTextBox.Text = sf.FileName;
                UpdateCreateButton();
            }
        }   

        private void UpdateCreateButton()
        {
            if (!string.IsNullOrWhiteSpace(Name) && FilesToEncrypt != null)
            {
                CreateButton.Enabled = true;
            }
            else
            {
                CreateButton.Enabled = false;
            }
        }

        private static string ConvertStringArrayToString(string[] array)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string value in array)
            {
                builder.Append(value);
                builder.Append(", ");
            }
            return builder.ToString();
        }
    }
}
